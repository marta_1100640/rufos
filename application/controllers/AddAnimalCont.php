<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddAnimalCont extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        $this->load->model('animalmodel');
    } 

	public function index(){
		$data["especies"] = $this->getEspecies();
		$data["sexos"] = $this->getSexos();
		$data["portes"] = $this->getPortes();
		$data["orelhas"] = $this->getOrelhas();
		$data["caudas"] = $this->getCaudas();
		$data["comprimentopelos"] = $this->getComprimentoPelos();
		$data["tipopelos"] = $this->getTipoPelos();
		$data["padraocores"] = $this->getPadraoCores();
		$data["focinhos"] = $this->getFocinhos();
		$data["olhos"] = $this->getOlhos();
		$data["cores"] = $this->getCores();
		$data["temperamentos"] = $this->getTemperamentos();
		$data["treinos"] = $this->getTreinos();
		$data["situacoesclinicas"] = $this->getSituacoesClinicas();
		$this->load->view('addanimal', $data);
	}

	public function create()
	{
	    $this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');

	    //$data['title'] = 'Inserir um animal novo';


	    $this->form_validation->set_rules('nome', 'Nome', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
	        $this->load->view('addanimal');

	    }
	    else
	    {
	    	$idanimal = $this->animalmodel->insertAnimal();
	    	//---------------------------------------

	    	/*$config['upload_path']		= './assets/fotos_animais/';
           	$config['allowed_types']	= 'jpg|png';
            $config['file_name']		= $idanimal;
            //$config['overwrite']		= TRUE;
            //$config['encrypt_name'] 	= FALSE;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        //$this->load->view('upload_form', $error);
                        print_r($error); die();
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                        print_r($data); die();
                }*/

            if (!empty($_FILES['foto']['name'][0])) {
                if ($this->upload_foto($_FILES['foto'], $idanimal) === FALSE) {
                    $data['error'] = $this->upload->display_errors('<div class="alert alert-danger">', '</div>');
                    print_r($data); die();
                }
            } 

	    	//---------------------------------------
	        //$this->animalmodel->insertAnimal();
	        $data["animais"] = $this->animalmodel->getAll();
	        //$this->load->view('home', $data);
	        redirect("home", $data);
	    }
	}

	private function upload_foto($files, $idanimal)
    {
        $config = array(
            'upload_path'   => './assets/fotos_animais/',
            'allowed_types' => 'jpg|jpeg|png',
            'overwrite'     => 1,                       
        );

        $this->load->library('upload', $config);

        $foto = array();
        $numero = $this->animalmodel->getNumeroFotosAnimalbyID($idanimal);

        foreach ($files['name'] as $key => $image) {
            $_FILES['foto[]']['name']= $files['name'][$key];
            $_FILES['foto[]']['type']= $files['type'][$key];
            $_FILES['foto[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['foto[]']['error']= $files['error'][$key];
            $_FILES['foto[]']['size']= $files['size'][$key];

            $numero++;
            $tipo = explode(".",$files['name'][$key]);
            
            $fileName = $idanimal .'_'. $numero . "." . $tipo[1];

            $this->animalmodel->insertFotoAnimal($idanimal, $fileName);

            $foto[] = $fileName;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('foto[]')) {
                $this->upload->data();
            } else {
                return false;
            }
        }

        return $foto;
    }

	public function getEspecies(){
		return $this->animalmodel->getEspecies();
	}

	public function getSexos(){
		return $this->animalmodel->getSexos();
	}

	public function getPortes(){
		return $this->animalmodel->getPortes();
	}

	public function getOrelhas(){
		return $this->animalmodel->getOrelhas();
	}

	public function getCaudas(){
		return $this->animalmodel->getCaudas();
	}

	public function getComprimentoPelos(){
		return $this->animalmodel->getComprimentoPelos();
	}

	public function getTipoPelos(){
		return $this->animalmodel->getTipoPelos();
	}

	public function getPadraoCores(){
		return $this->animalmodel->getPadraoCores();
	}

	public function getFocinhos(){
		return $this->animalmodel->getFocinhos();
	}

	public function getOlhos(){
		return $this->animalmodel->getOlhos();
	}

	public function getCores(){
		return $this->animalmodel->getCores();
	}

	public function getTemperamentos(){
		return $this->animalmodel->getTemperamentos();
	}

	public function getTreinos(){
		return $this->animalmodel->getTreinos();
	}

	public function getSituacoesClinicas(){
		return $this->animalmodel->getSituacoesClinicas();
	}

	//$this->load->model('model_name');
	//$this->model_name->method();

	//$autoload['model'] = array('first_model', 'second_model');
}

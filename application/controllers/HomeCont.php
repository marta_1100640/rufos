<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeCont extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        $this->load->model('animalmodel');
    } 

	public function index(){
		//$data["animais"] = $this->getAll();
		$data["animais"] = $this->animalmodel->getRandomAnimals(2);
		$data["fotos"] = $this->animalmodel->getFotosAnimalsbyID($data);
		//print_r($data); die;
		//echo "<pre>";  print_r($data["animais"]); echo "</pre>"; die;
		$this->load->view('home', $data);
	}

	public function insertAnimal($nome){
		echo $this->animalmodel->insert($nome);
	}

	public function deleteAnimal($id){
		$this->animalmodel->getFotoAnimalbyID($id);
		$fotos = $this->animalmodel->getFotoAnimal();
		//$path = realpath("./assets/fotos_animais");
		
		//if(is_writable($path)){
			foreach($fotos as $foto){
				unlink("./assets/fotos_animais/".$foto);
			}
		//}
		//print_r($fotos[0]); die;
		//echo "fotos: "; echo $fotos; die;
		//$base = base_url();
		//foreach($fotos as $foto){

			//echo base_url();
			//return "fotos: ". $fotos . "foto: "$foto;
			//print_r(base_url()."assets/fotos_animais/".$foto); die;
			//unlink("./assets/fotos_animais/".$foto);
		//}
		$this->animalmodel->delete($id);
	}

	public function getAll(){
		return $this->animalmodel->getAll();
	}

	//$this->load->model('model_name');
	//$this->model_name->method();

	//$autoload['model'] = array('first_model', 'second_model');
}

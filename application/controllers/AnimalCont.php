<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnimalCont extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        $this->load->model('animalmodel');
        $this->load->helper('url');
    }

	public function index($id_animal){
		$this->getAnimalbyID($id_animal);
		$data["id"] = $this->animalmodel->getID();
		$data["nome"] = $this->animalmodel->getNome();
		$data["ongid"] = $this->animalmodel->getONGID();
		$data["ongnome"] = $this->animalmodel->getONGNome();
		$data["localizacao"] = $this->animalmodel->getLocalizacao();
		$data["especie"] = $this->animalmodel->getEspecie();
		$data["sexo"] = $this->animalmodel->getSexo();
		$data["datanascimento"] = $this->animalmodel->getDataNascimento();
		$data["estado"] = $this->animalmodel->getEstado();
		$data["adotado"] = $this->animalmodel->getAdotado();
		$data["porte"] = $this->animalmodel->getPorte();
		$data["orelhas"] = $this->animalmodel->getOrelha();
		$data["cauda"] = $this->animalmodel->getCauda();
		$data["comprimentopelo"] = $this->animalmodel->getComprimentoPelo();
		$data["tipopelo"] = $this->animalmodel->getTipoPelo();
		$data["padraocor"] = $this->animalmodel->getPadraoCor();
		$data["focinho"] = $this->animalmodel->getFocinho();
		$data["olhos"] = $this->animalmodel->getOlho();
		$data["descricao"] = $this->animalmodel->getDescricao();

		$data["cor"] = $this->animalmodel->getCor();
		$data["temperamento"] = $this->animalmodel->getTemperamento();
		$data["treino"] = $this->animalmodel->getTreino();
		$data["situacaoclinica"] = $this->animalmodel->getSituacaoClinica();
		$data["fotoanimal"] = $this->animalmodel->getFotoAnimal();

		//print_r($data["animal"]->getNome()[0]->ong[0]->Nome); die;
		//$data["ong"]= $this->getONGbyID($data["animal"][0]->ID_ONG);
		//$data["especie"]= $this->getEspeciebyID($data["animal"][0]->);
		$this->load->view('animal', $data);
	}

	public function getAnimalbyID($id_animal){
		return $this->animalmodel->getAnimalbyID($id_animal);
	}

	/*public function getONGbyID($id_ong){
		return $this->animalmodel->getONGbyID($id_ong);
	}*/

	//$this->load->model('model_name');
	//$this->model_name->method();

	//$autoload['model'] = array('first_model', 'second_model');
}

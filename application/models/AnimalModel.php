<?php 
   Class AnimalModel extends CI_Model { 
	
      private $id;
      private $nome;
      private $ong_id;
      private $ong_nome;
      private $localizacao;
      private $especie;
      private $sexo;
      private $datanascimento;
      private $estado;
      private $adotado;
      private $porte;
      private $orelhas;
      private $cauda;
      private $comprimentopelo;
      private $tipopelo;
      private $padraocor;
      private $focinho;
      private $olhos;
      private $descricao;

      private $cor;
      private $temperamento;
      private $situacaoclinica;
      private $treino;
      private $foto;

      Public function __construct() { 
         parent::__construct(); 

         $this->load->database();
         $this->load->helper(array('form', 'url'));
      } 

      /*public function insert($nome){
      	$data = array(
      		"ID_ONG" => 1,
      		'Nome' => $nome,
      		'Especie' => 1,
      		'Sexo' => 1,
      		'DataNascimento' => "2016-08-04",
      		'Porte' => 1,
      		'Orelhas' => 1,
      		'Cauda' => 1,
      		'ComprimentoPelo' => 1,
      		'TipoPelo' => 1,
      		'PadraoCor' => 1,
      		'Focinho' => 1,
      		'Olhos' => 1,
      		'Descricao' => "x"
	        //'title' => $this->input->post('title'),
	        //'slug' => $slug,
	        //'text' => $this->input->post('text')
	      );

	    $this->db->insert('animal', $data);
      }*/

      public function insertFotoAnimal($idanimal, $filename){
         $data = array(
            'ID_Animal' => $idanimal,
            'Foto' => $filename
         );
         $this->db->insert('fotoanimal', $data);
      }

      public function insertAnimal(){

         //print_r($this->input->post('cor')); die();
         $today = getdate();

         $data1 = array(
            'ID_ONG' => 1,
            'Nome' => $this->input->post('nome'),
            'Especie' => $this->input->post('especie'),
            'Sexo' => $this->input->post('sexo'),
            'DataNascimento' => $this->input->post('data'),
            'Porte' => $this->input->post('porte'),
            'Orelhas' => $this->input->post('orelhas'),
            'Cauda' => $this->input->post('cauda'),
            'ComprimentoPelo' => $this->input->post('comprimentopelo'),
            'TipoPelo' => $this->input->post('tipopelo'),
            'PadraoCor' => $this->input->post('padraocor'),
            'Focinho' => $this->input->post('focinho'),
            'Olhos' => $this->input->post('olhos'),
            'Descricao' => $this->input->post('descricao'),
            'DataInsercao' => $today["year"]."-".$today["mon"]."-".$today["mday"]." ".$today["hours"].":".$today["minutes"].":".$today["seconds"],
            'DataUpdate' => $today["year"]."-".$today["mon"]."-".$today["mday"]." ".$today["hours"].":".$today["minutes"].":".$today["seconds"]
         );

         //print_r($this->input->post('nome')); die();
         $this->db->insert('animal', $data1);

         $idanimal = $this->db->insert_id();
         $cores = $this->input->post('cor');

         foreach($cores as $cor){
            $data2 = array(
               'ID_Animal' => $idanimal,
               'ID_TipoCor' => $cor
            );
            $this->db->insert('cor', $data2);
         }

         $temperamentos = $this->input->post('temperamento');
         foreach($temperamentos as $temperamento){
            $data3 = array(
               'ID_Animal' => $idanimal,
               'ID_TipoTemperamento' => $temperamento
            );
            $this->db->insert('temperamento', $data3);
         }

         $treinos = $this->input->post('treino');
         foreach($treinos as $treino){
            $data4 = array(
               'ID_Animal' => $idanimal,
               'ID_TipoTreino' => $treino
            );
            $this->db->insert('treino', $data4);
         }

         $situacoesclinicas = $this->input->post('situacaoclinica');
         foreach($situacoesclinicas as $situacaoclinica){
            $data5 = array(
               'ID_Animal' => $idanimal,
               'ID_TipoSituacaoClinica' => $situacaoclinica
            );
            $this->db->insert('situacaoclinica', $data5);
         }

         //FOTO

         // Count # of uploaded files in array
         /*$total = count($_FILES['upload']['name']);
         print_r($total); die();
         // Loop through each file
         for($i=0; $i<$total; $i++) {
           //Get the temp file path
           $tmpFilePath = $_FILES['upload']['tmp_name'][$i];

           //Make sure we have a filepath
           if ($tmpFilePath != ""){
             //Setup our new file path
             $newFilePath = "../assets/fotos_animais/" . $_FILES['upload']['name'][$i];
             print_r("entrou"); die();
             //Upload the file into the temp dir
             if(move_uploaded_file($tmpFilePath, $newFilePath)) {

               $data6 = array(
                  'ID_Animal' => $idanimal,
                  'Foto' => $newFilePath
               );
               $this->db->insert('fotoanimal', $data6);

             }
           }
         }*/
         //$this->load->helper(array('form', 'url'));

         //$config['upload_path']          = 'C:\wamp\www\rufos\assets\fotos_animais';
         //$config['allowed_types']        = 'jpg|jpeg|png';
         //$config['file_name'] = $idanimal;
         /*$config['max_size']             = 1024;
         $config['max_width']            = 1024;
         $config['max_height']           = 768;*/

         //$this->load->library('upload', $config);

         /*if (!empty($_FILES['foto']['name'][0])) {
            if ($this->upload_files($idanimal, $_FILES['foto']) === FALSE) {
               //$data['error'] = $this->upload->display_errors('<div class="alert alert-danger">', '</div>');
               print_r("ERRO"); die();
            }
         } */

         /*$image = $_FILES['foto'];
         if (!empty($image)) {
            print_r($image); die();
            //foreach ($_FILES['foto']['name'] as $key => $image) {
               //print_r("entrou2"); die();
               $_FILES['foto']['name']= $files['name'][$key];
               $_FILES['foto']['type']= $files['type'][$key];
               $_FILES['foto']['tmp_name']= $files['tmp_name'][$key];
               $_FILES['foto']['error']= $files['error'][$key];
               $_FILES['foto']['size']= $files['size'][$key];

               $this->upload->initialize($config);

               if ($this->upload->do_upload($image)) {
                  $this->upload->data();
               } else {
                  print_r("ERRO"); die();
               }
            //}
         }*/
         /*$fotos = $this->input->post('foto');
         print_r($fotos); die();
         foreach($fotos as $foto){
            if ( ! $this->upload->do_upload($foto)){
               $error = array('error' => $this->upload->display_errors());
               print_r($error); die();
               //$this->load->view('upload_form', $error);
            }else{
               $dados = array('upload_data' => $this->upload->data());
               print_r($dados); die();
               //$this->load->view('upload_success', $data);
            }
         }*/
         return $idanimal;
      }

      public function delete($id){
         $this->db->delete('animal', array('ID_Animal' => $id));
         $this->db->delete('fotoanimal', array('ID_Animal' => $id));
         $this->db->delete('cor', array('ID_Animal' => $id));
         $this->db->delete('temperamento', array('ID_Animal' => $id));
         $this->db->delete('treino', array('ID_Animal' => $id));
         $this->db->delete('situacaoclinica', array('ID_Animal' => $id));
      }

      public function getAnimalbyID($id_animal){
         $this->db->from("animal");
         $this->db->where('ID_Animal', $id_animal);
         $query = $this->db->get();
         $animal = $query->result();

         $this->id = $animal[0]->ID_Animal;
         $this->nome = $animal[0]->Nome;

         $this->load->model('ongmodel');
         $this->ong_id = $animal[0]->ID_ONG;
         $this->getONGbyID($animal[0]->ID_ONG);

         $this->getEspeciebyID($animal[0]->Especie);
         $this->getSexobyID($animal[0]->Sexo);

         $this->datanascimento = $animal[0]->DataNascimento;
         $this->estado = $animal[0]->Estado;
         $this->adotado = $animal[0]->Adotado;

         $this->getPortebyID($animal[0]->Porte);
         $this->getOrelhasbyID($animal[0]->Orelhas);
         $this->getCaudabyID($animal[0]->Cauda);
         $this->getComprimentoPelobyID($animal[0]->ComprimentoPelo);
         $this->getTipoPelobyID($animal[0]->TipoPelo);
         $this->getPadraoCorbyID($animal[0]->PadraoCor);
         $this->getFocinhobyID($animal[0]->Focinho);
         $this->getOlhosbyID($animal[0]->Olhos);

         $this->descricao = $animal[0]->Descricao;


         $this->db->from("cor");
         $this->db->where('ID_Animal', $id_animal);
         $query = $this->db->get();
         $cor = $query->result();

         $this->getCorbyID($cor);

         $this->db->from("temperamento");
         $this->db->where('ID_Animal', $id_animal);
         $query = $this->db->get();
         $temperamento = $query->result();

         $this->getTemperamentobyID($temperamento);

         $this->db->from("treino");
         $this->db->where('ID_Animal', $id_animal);
         $query = $this->db->get();
         $treino = $query->result();

         $this->getTreinobyID($treino);

         $this->db->from("situacaoclinica");
         $this->db->where('ID_Animal', $id_animal);
         $query = $this->db->get();
         $situacaoclinica = $query->result();

         $this->getSituacaoClinicabyID($situacaoclinica);

         $this->getFotoAnimalbyID($id_animal);

         //$ong = $this->getONGbyID($animal[0]->ID_ONG);

         //$animal[0]->nomeEspecie = $this->getEspecieByID($animal[0]->Especie);

         /*$data = array(
            'nome' => 'My Title',
            'ong' => $ong[0]->Nome
         );*/
      }


      public function getONGbyID($id){
         $this->db->from("ong");
         $this->db->where('ID_ONG', $id);
         $query = $this->db->get();
         $nomeong = $query->result();
         $this->ong_nome = $nomeong[0]->Nome;
         $this->localizacao = $nomeong[0]->Abrigo;
      }

      public function getEspeciebyID($id){
         $this->db->from("especie");
         $this->db->where('ID_Especie', $id);
         $query = $this->db->get();
         $nomeespecie = $query->result();
         $this->especie = $nomeespecie[0]->Especie;
      }

      public function getSexobyID($id){
         $this->db->from("sexo");
         $this->db->where('ID_Sexo', $id);
         $query = $this->db->get();
         $nomesexo = $query->result();
         $this->sexo = $nomesexo[0]->Sexo;
      }

      public function getPortebyID($id){
         $this->db->from("porte");
         $this->db->where('ID_Porte', $id);
         $query = $this->db->get();
         $nomeporte = $query->result();
         $this->porte = $nomeporte[0]->Porte;
      }

      public function getOrelhasbyID($id){
         $this->db->from("orelhas");
         $this->db->where('ID_Orelhas', $id);
         $query = $this->db->get();
         $nomeorelhas = $query->result();
         $this->orelhas = $nomeorelhas[0]->Orelhas;
      }

      public function getCaudabyID($id){
         $this->db->from("cauda");
         $this->db->where('ID_Cauda', $id);
         $query = $this->db->get();
         $nomecauda = $query->result();
         $this->cauda = $nomecauda[0]->Cauda;
      }

      public function getComprimentoPelobyID($id){
         $this->db->from("comprimentopelo");
         $this->db->where('ID_ComprimentoPelo', $id);
         $query = $this->db->get();
         $nomecomprimentopelo = $query->result();
         $this->comprimentopelo = $nomecomprimentopelo[0]->ComprimentoPelo;
      }

      public function getTipoPelobyID($id){
         $this->db->from("tipopelo");
         $this->db->where('ID_TipoPelo', $id);
         $query = $this->db->get();
         $nometipopelo = $query->result();
         $this->tipopelo = $nometipopelo[0]->TipoPelo;
      }

      public function getPadraoCorbyID($id){
         $this->db->from("padraocor");
         $this->db->where('ID_PadraoCor', $id);
         $query = $this->db->get();
         $nomepadraocor = $query->result();
         $this->padraocor = $nomepadraocor[0]->PadraoCor;
      }

      public function getFocinhobyID($id){
         $this->db->from("focinho");
         $this->db->where('ID_Focinho', $id);
         $query = $this->db->get();
         $nomefocinho = $query->result();
         $this->focinho = $nomefocinho[0]->Focinho;
      }

      public function getOlhosbyID($id){
         $this->db->from("olhos");
         $this->db->where('ID_Olhos', $id);
         $query = $this->db->get();
         $nomeolhos = $query->result();
         $this->olhos = $nomeolhos[0]->Olhos;
      }

      public function getCorbyID($id){
         $contador=0;
         foreach($id as $c){
            $this->db->from("tipocor");
            $this->db->where('ID_TipoCor', $c->ID_TipoCor);
            $query = $this->db->get();
            $tipocor = $query->result();
            $this->cor[$contador] = $tipocor[0]->Cor;

            $contador ++;
         }
      }

      public function getTemperamentobyID($id){
         $contador=0;
         foreach($id as $t){
            $this->db->from("tipotemperamento");
            $this->db->where('ID_TipoTemperamento', $t->ID_TipoTemperamento);
            $query = $this->db->get();
            $tipotemperamento = $query->result();
            $this->temperamento[$contador] = $tipotemperamento[0]->Temperamento;

            $contador ++;
         }
      }

      public function getTreinobyID($id){
         $contador=0;
         foreach($id as $t){
            $this->db->from("tipotreino");
            $this->db->where('ID_TipoTreino', $t->ID_TipoTreino);
            $query = $this->db->get();
            $tipotreino = $query->result();
            $this->treino[$contador] = $tipotreino[0]->TipoTreino;

            $contador ++;
         }
      }

      public function getSituacaoClinicabyID($id){
         $contador=0;
         foreach($id as $sc){
            $this->db->from("tiposituacaoclinica");
            $this->db->where('ID_TipoSituacaoClinica', $sc->ID_TipoSituacaoClinica);
            $query = $this->db->get();
            $tiposituacaoclinica = $query->result();
            $this->situacaoclinica[$contador] = $tiposituacaoclinica[0]->TipoSituacaoClinica;

            $contador ++;
         }
      }

      public function getFotoAnimalbyID($id_animal){
         $this->db->from("fotoanimal");
         $this->db->where('ID_Animal', $id_animal);
         $query = $this->db->get();
         $fotoanimal = $query->result();

         $contador=0;
         foreach($fotoanimal as $fa){
            $this->foto[$contador] = $fa->Foto;

            $contador ++;
         }
      }

      public function getFotosAnimalsbyID($animais){
         //$lalala = json_encode($animais);
         //print_r($this->$animais["animais"]); die;
         $fotos=array();
         foreach($animais["animais"] as $animal){
            $id_animal = $animal->ID_Animal;
            //print_r($animal); die;
            $this->db->from("fotoanimal");
            $this->db->where('ID_Animal', $id_animal);
            $query = $this->db->get();
            $fotoanimal = $query->result();

            $cont=0;
            foreach($fotoanimal as $fotoA){
               if($cont==0){
                  $fotos[$id_animal] = $fotoA->Foto;
               }
               $cont++;
            }
         }
         //if(isset($fotos)){
            //json_encode($fotos);
            return $fotos;
         //}

         /*$this->db->from("fotoanimal");
         $this->db->where('ID_Animal', $id_animais);
         $query = $this->db->get();
         $fotoanimal = $query->result();

         $contador=0;
         foreach($fotoanimal as $fa){
            $this->foto[$contador] = $fa->Foto;

            $contador ++;
         }*/
      }

      public function getNumeroFotosAnimalbyID($id){
         $this->db->from("fotoanimal");
         $this->db->where('ID_Animal', $id);
         return $this->db->count_all_results();
      }
      

      public function getAnimalbyName($nome){
         $this->db->from("animal");
         $this->db->like('Nome', $nome, 'both');
         $query = $this->db->get();
         return $query->result();
      }

      public function getRandomAnimals($limit) {
           $total = $this->db->count_all('animal');

           //Get random values. Speed optimization by predetermine random rownumbers using php

           $arr = array();
           while (count($arr) < $limit) { 
             $x = mt_rand(1, $total); //get random value between limit and 0
             if (!isset($arr[$x])) { //Random value must be unique
               //using random value as key and check using isset is faster then in_array
               $arr[$x] = true; 
             }
           }

           //Create IN string
           $in = implode(',', array_keys($arr));

           //Selection based on random rownumbers
           $query = $this->db->query('SELECT * FROM
               (SELECT  @row := @row + 1 as row, t.*
                  FROM `animal` t, (SELECT @row := 0) r) AS tracks
              WHERE `row` IN(' . $in . ')');

           return $query->result();
      }

      //Gets dos atributos todos da base de dados

      public function getAll(){
         $query = $this->db->get("animal");
         return $query->result();
      }

      public function getEspecies(){
         $query = $this->db->get("especie");
         return $query->result();
      }

      public function getSexos(){
         $query = $this->db->get("sexo");
         return $query->result();
      }

      public function getPortes(){
         $query = $this->db->get("porte");
         return $query->result();
      }

      public function getOrelhas(){
         $query = $this->db->get("orelhas");
         return $query->result();
      }

      public function getCaudas(){
         $query = $this->db->get("cauda");
         return $query->result();
      }

      public function getComprimentoPelos(){
         $query = $this->db->get("comprimentopelo");
         return $query->result();
      }

      public function getTipoPelos(){
         $query = $this->db->get("tipopelo");
         return $query->result();
      }

      public function getPadraoCores(){
         $query = $this->db->get("padraocor");
         return $query->result();
      }

      public function getFocinhos(){
         $query = $this->db->get("focinho");
         return $query->result();
      }

      public function getOlhos(){
         $query = $this->db->get("olhos");
         return $query->result();
      }

      public function getCores(){
         $query = $this->db->get("tipocor");
         return $query->result();
      }

      public function getTemperamentos(){
         $query = $this->db->get("tipotemperamento");
         return $query->result();
      }

      public function getTreinos(){
         $query = $this->db->get("tipotreino");
         return $query->result();
      }

      public function getSituacoesClinicas(){
         $query = $this->db->get("tiposituacaoclinica");
         return $query->result();
      }

      //Gets dos parametros da classe

      public function getID(){
         return $this->id;
      }

      public function getNome(){
         return $this->nome;
      }

      public function getONGID(){
         return $this->ong_id;
      }

      public function getONGNome(){
         return $this->ong_nome;
      }

      public function getLocalizacao(){
         return $this->localizacao;
      }

      public function getEspecie(){
         return $this->especie;
      }

      public function getSexo(){
         return $this->sexo;
      }

      public function getDataNascimento(){
         return $this->datanascimento;
      }

      public function getEstado(){
         return $this->estado;
      }

      public function getAdotado(){
         return $this->adotado;
      }

      public function getPorte(){
         return $this->porte;
      }

      public function getOrelha(){
         return $this->orelhas;
      }

      public function getCauda(){
         return $this->cauda;
      }

      public function getComprimentoPelo(){
         return $this->comprimentopelo;
      }

      public function getTipoPelo(){
         return $this->tipopelo;
      }

      public function getPadraoCor(){
         return $this->padraocor;
      }

      public function getFocinho(){
         return $this->focinho;
      }

      public function getOlho(){
         return $this->olhos;
      }

      public function getDescricao(){
         return $this->descricao;
      }

      public function getCor(){
         return $this->cor;
      }

      public function getTemperamento(){
         return $this->temperamento;
      }

      public function getTreino(){
         return $this->treino;
      }

      public function getSituacaoClinica(){
         return $this->situacaoclinica;
      }

      public function getFotoAnimal(){
         return $this->foto;
      }

      //$this->db->close();
   } 
?> 
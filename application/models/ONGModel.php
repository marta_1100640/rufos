<?php 
   Class ONGModel extends CI_Model { 
	
      Public function __construct() { 
         parent::__construct(); 

         $this->load->database();
      } 

      /*public function insert($nome){
      	$data = array(
      		"ID_ONG" => 1,
      		'Nome' => $nome,
      		'Especie' => 1,
      		'Sexo' => 1,
      		'DataNascimento' => "2016-08-04",
      		'Porte' => 1,
      		'Orelhas' => 1,
      		'Cauda' => 1,
      		'ComprimentoPelo' => 1,
      		'TipoPelo' => 1,
      		'PadraoCor' => 1,
      		'Focinho' => 1,
      		'Olhos' => 1,
      		'Descricao' => "x"
	        //'title' => $this->input->post('title'),
	        //'slug' => $slug,
	        //'text' => $this->input->post('text')
	      );

	    $this->db->insert('animal', $data);
      }*/

      public function delete($id){
         $this->db->delete('ong', array('ID_ONG' => $id));
      }

      public function getAll(){
      	$query = $this->db->get("ong");
      	return $query->result();
      }

      public function getONGbyID($id_ong){
         $this->db->from("ong");
         $this->db->where('ID_ONG', $id_ong);
         $query = $this->db->get();
         return $query->result();
      }
   } 
?> 
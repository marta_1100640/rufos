<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
		<style>
			html { 
			  background: url(../assets/cat.jpg) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
			}

			#corners {
				border-radius: 25px;
				background: #73AD21;
				padding: 20px;
				width: 200px;
				height: 150px;
			}

			#caixa{			
				border-radius: 10px;
				color: #1B3C52;
				background: #FFFFFF;
				padding: 10px; 
				width: 110px;
				height: 20px; 
				opacity: 0.3;
				filter: alpha(opacity=30); /* For IE8 and earlier */
				
				position: fixed;
				top: 10px;
				left: 10px;
			}
			
			#homepageText{
				font-family: "Verdana";
				font-size: 15px;
				text-align: center;
			}
		</style>
	</head>
	<body>
		<div id="caixa">
			<span id="homepageText">Login</span>
			<button type="button" class="btn btn-default" aria-label="Left Align">
				<span class="glyphicon glyphicon-align-left" aria-hidden="true"></span>
			</button>
		</div>
		<!-- <div id="caixa">
			<p id="homepageText">Sobre o Rufos</p>
		</div> -->
	</body>
</html>
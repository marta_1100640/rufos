<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>Bootstrap 101 Template</title>

	    <!-- Bootstrap -->
	    <link href="/rufos/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <link rel="stylesheet" href="/rufos/assets/css/stylesheet.css" type="text/css" media="screen"/>

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>
  	<body>
  		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#"><img src='/rufos/assets/icons/rufos.png' style='height: 100%; width: 100%;'></a>
		    </div>
		    <ul class="nav navbar-nav navbar-right">
		      <li>
		      	<a href="/rufos/index.php/home">
		      		<img src='/rufos/assets/icons/ic_home_white_48dp_1x.png' data-toggle="tooltip" title="Página principal" style='height: 35px; width: auto; '/>
		      	</a>
		      </li>
		      <li>
		      	<a href="/rufos/index.php/pesquisa/associacao">
		      		<div class='esp'></div>
		      		<img src='/rufos/assets/icons/iconathon_animal-shelter_simple-black_48x48.png' data-toggle="tooltip" title="Pesquisar associação" style='height: 28px; width: auto; '/>
		      	</a>
		      </li>
		      <li>
		      	<a href="/rufos/index.php/pesquisa/animal">
		      		<div class='esp2'></div>
		      		<img src='/rufos/assets/icons/ic_pets_white_48dp_1x.png' data-toggle="tooltip" title="Pesquisar animal" style='height: 30px; width: auto; '/>
		      	</a>
		      </li>
		      <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown">
		        	<div class='esp'></div>
		        	<img src='/rufos/assets/icons/ic_account_circle_white_48dp_1x.png' data-toggle="tooltip" title="Conta" style='height: 30px; width: auto; '/>
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		          <li><a href="#">Notificações</a></li>
		          <li><a href="#">Favoritos</a></li>
		          <li><a href="#">Editar perfil</a></li>
		        </ul>
		      </li>
		      <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown">
		        	<img src='/rufos/assets/icons/ic_supervisor_account_white_48dp_1x.png' data-toggle="tooltip" title="Associação" style='height: 36px; width: auto; '/>
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		          <li><a href="#">Notificações</a></li>
		          <li><a href="/rufos/index.php/adicionar/animal">Adicionar animal</a></li>
		          <li><a href="#">Gerir animais</a></li>
		          <li><a href="#">Criar notícia</a></li>
		          <li><a href="#">Criar apelo externo</a></li>
		          <li><a href="#">Gerir cargos</a></li>
		          <li><a href="#">Editar perfil</a></li>
		        </ul>
		      </li>
		    </ul>
		  </div>
		</nav>

  		<div class="container-fluid">
		    <div class="row">
		    	<div class="col-md-4" id="c1">
					Coluna 1
					</br>
					</br>
					<a href="http://localhost/rufos/index.php/search">Pesquisar</a>
					</br>
					<a href="http://localhost/rufos/index.php/myong">My ONG</a>
				</div>
				<div class="col-md-8" id="c2">
					MY ONG
					</br>

					<?php 
					$this->load->helper('form');
					echo validation_errors();
					?>

					<?php
						//$hidden = array('ong' => , 'ong_id' => );
						//echo form_open('AddAnimalCont/create', '', $hidden);	
						echo form_open_multipart('AddAnimalCont/create'); ?>

						</br>
					    <label for="nome">Nome</label>
					    <input type="input" name="nome" /></br></br>

					    <label for="especie">Especie</label>
					    <select name="especie">
					    	<?php
								if(isset($especies)){
									foreach($especies as $e){
										echo "<option value='".$e->ID_Especie."'>".$e->Especie."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select> 

						<label for="sexo">Sexo</label>
					    <select name="sexo">
							<?php
								if(isset($sexos)){
									foreach($sexos as $s){
										echo "<option value='".$s->ID_Sexo."'>".$s->Sexo."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select> 

						<label for="data">Data de Nascimento (0000-00-00)</label>
					    <input type="input" name="data" /><br />

					    <label for="porte">Porte</label>
					    <select name="porte">
							<?php
								if(isset($portes)){
									foreach($portes as $p){
										echo "<option value='".$p->ID_Porte."'>".$p->Porte."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						<label for="orelhas">Orelhas</label>
					    <select name="orelhas">
							<?php
								if(isset($orelhas)){
									foreach($orelhas as $o){
										echo "<option value='".$o->ID_Orelhas."'>".$o->Orelhas."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						<label for="cauda">Cauda</label>
					    <select name="cauda">
							<?php
								if(isset($caudas)){
									foreach($caudas as $c){
										echo "<option value='".$c->ID_Cauda."'>".$c->Cauda."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						<label for="comprimentopelo">Comprimento do Pêlo</label>
					    <select name="comprimentopelo">
							<?php
								if(isset($comprimentopelos)){
									foreach($comprimentopelos as $cp){
										echo "<option value='".$cp->ID_ComprimentoPelo."'>".$cp->ComprimentoPelo."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						<label for="tipopelo">Tipo do Pêlo</label>
					    <select name="tipopelo">
							<?php
								if(isset($tipopelos)){
									foreach($tipopelos as $tp){
										echo "<option value='".$tp->ID_TipoPelo."'>".$tp->TipoPelo."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						<label for="padraocor">Padrão da Cor</label>
					    <select name="padraocor">
							<?php
								if(isset($padraocores)){
									foreach($padraocores as $pc){
										echo "<option value='".$pc->ID_PadraoCor."'>".$pc->PadraoCor."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						<label for="focinho">Focinho</label>
					    <select name="focinho">
							<?php
								if(isset($focinhos)){
									foreach($focinhos as $f){
										echo "<option value='".$f->ID_Focinho."'>".$f->Focinho."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						<label for="olhos">Olhos</label>
					    <select name="olhos">
							<?php
								if(isset($olhos)){
									foreach($olhos as $ol){
										echo "<option value='".$ol->ID_Olhos."'>".$ol->Olhos."</option>";
										//<p id='".$e->ID_Animal."' >".$e->Nome;
									}
								}
							?>
						</select>

						</br></br>
						<label for="cor[]">Cores</label>
							<?php
								if(isset($cores)){
									foreach($cores as $co){
										echo "</br><input type='checkbox' name='cor[]' value='".$co->ID_TipoCor."'> ".$co->Cor;
									}
								}
							?>

						</br></br>
						<label for="temperamento[]">Temperamento</label>
							<?php
								if(isset($temperamentos)){
									foreach($temperamentos as $te){
										echo "</br><input type='checkbox' name='temperamento[]' value='".$te->ID_TipoTemperamento."'> ".$te->Temperamento;
									}
								}
							?>

						</br></br>
						<label for="treino[]">Treinado</label>
							<?php
								if(isset($treinos)){
									foreach($treinos as $tr){
										echo "</br><input type='checkbox' name='treino[]' value='".$tr->ID_TipoTreino."'> ".$tr->TipoTreino;
									}
								}
							?>

						</br></br>
						<label for="situacaoclinica[]">Situação clinica</label>
							<?php
								if(isset($situacoesclinicas)){
									foreach($situacoesclinicas as $sc){
										echo "</br><input type='checkbox' name='situacaoclinica[]' value='".$sc->ID_TipoSituacaoClinica."'> ".$sc->TipoSituacaoClinica;
									}
								}
							?>

						</br></br>
					    <label for="descricao">Descricao</label></br>
					    <textarea name="descricao"></textarea><br />

					    </br></br>
					    <label for="foto[]">Foto</label></br>
					    <input type="file" name="foto[]" multiple/>

					    </br></br>
					    <input type="submit" name="submit" value="OK" />

					</form>
				</div>
			</div>
		</div>

	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="/rufos/assets/bootstrap/js/bootstrap.min.js"></script>
	    <script src="/rufos/assets/JSfunctions.js"></script>
  	</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>Bootstrap 101 Template</title>

	    <!-- Bootstrap -->
	    <link href="/rufos/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <link rel="stylesheet" href="/rufos/assets/css/stylesheet.css" type="text/css" media="screen"/>


	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>
  	<body>
  		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#"><img src='/rufos/assets/icons/rufos.png' style='height: 100%; width: 100%;'></a>
		    </div>
		    <ul class="nav navbar-nav navbar-right">
		      <li>
		      	<a href="/rufos/index.php/home">
		      		<img src='/rufos/assets/icons/ic_home_white_48dp_1x.png' data-toggle="tooltip" title="Página principal" style='height: 35px; width: auto; '/>
		      	</a>
		      </li>
		      <li>
		      	<a href="/rufos/index.php/pesquisa/associacao">
		      		<div class='esp'></div>
		      		<img src='/rufos/assets/icons/iconathon_animal-shelter_simple-black_48x48.png' data-toggle="tooltip" title="Pesquisar associação" style='height: 28px; width: auto; '/>
		      	</a>
		      </li>
		      <li>
		      	<a href="/rufos/index.php/pesquisa/animal">
		      		<div class='esp2'></div>
		      		<img src='/rufos/assets/icons/ic_pets_white_48dp_1x.png' data-toggle="tooltip" title="Pesquisar animal" style='height: 30px; width: auto; '/>
		      	</a>
		      </li>
		      <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown">
		        	<div class='esp'></div>
		        	<img src='/rufos/assets/icons/ic_account_circle_white_48dp_1x.png' data-toggle="tooltip" title="Conta" style='height: 30px; width: auto; '/>
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		          <li><a href="#">Notificações</a></li>
		          <li><a href="#">Favoritos</a></li>
		          <li><a href="#">Editar perfil</a></li>
		        </ul>
		      </li>
		      <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown">
		        	<img src='/rufos/assets/icons/ic_supervisor_account_white_48dp_1x.png' data-toggle="tooltip" title="Associação" style='height: 36px; width: auto; '/>
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		          <li><a href="#">Notificações</a></li>
		          <li><a href="/rufos/index.php/adicionar/animal">Adicionar animal</a></li>
		          <li><a href="#">Gerir animais</a></li>
		          <li><a href="#">Criar notícia</a></li>
		          <li><a href="#">Criar apelo externo</a></li>
		          <li><a href="#">Gerir cargos</a></li>
		          <li><a href="#">Editar perfil</a></li>
		        </ul>
		      </li>
		    </ul>
		  </div>
		</nav>
  	
  		<div class="container-fluid">
		    <div class="row">
		    	<!--
				<div class="col-md-2" id="c1">
					Coluna 1
					</br>
					</br>
					<a href="http://localhost/rufos/index.php/search">Pesquisar</a>
					</br>
					<a href="http://localhost/rufos/index.php/myong">My ONG</a>
				</div>
				-->
				<div class="col-md-12" id="c2">
					<?php
						if(isset($animais)){
							$cont=0;
							foreach($animais as $a){
								if($cont==0){
									echo "<div class='row'>";
								}

								echo "<div class='box1 col-md-2'>";
									$base = base_url();
  									echo "<div class='box2 background-image2' style='background-image: url(".$base."assets/fotos_animais/".$fotos[$a->ID_Animal]."); background-repeat: no-repeat; background-position: center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;'></div>";

									/*echo "<a href='http://localhost/rufos/index.php/animal/".$a->ID_Animal."' >";
										echo "<div class='crop'>
										    		<img src='/rufos/assets/fotos_animais/".$fotos[$a->ID_Animal]."' alt=''>
											</div>";*/

										/*echo "<div class='image'>
								            <img src='/rufos/assets/fotos_animais/".$fotos[$a->ID_Animal]."' class='img img-responsive full-width' />
								        </div>";*/


										//echo "<div class='background-image2' style='background: url(rufos/assets/fotos_animais/".$fotos[$a->ID_Animal].") no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;'>";
            							//echo "</div>";

										//echo "<img src='/rufos/assets/fotos_animais/".$fotos[$a->ID_Animal]."' class='img-responsive' />";
										//echo "<button class='deleteAnimal' id='".$a->ID_Animal."'>Eliminar animal</button>";
									//echo "</a>";
								echo "</div>";

								if($cont==5){
									echo "</div>";
									$cont=0;
								}else{
									$cont++;
								}
							}

							if($cont!=0){
								while($cont<6){
									echo "<div class='col-md-3'></div>";
									if($cont==5){
										echo "</div>";
									}
									$cont++;
								}
							}
							
						}
					?>





					<!--
					<div class="grid">
		  			<div class="grid-sizer"></div>
		  			
					
						if(isset($animais)){
							foreach($animais as $a){
								echo "<div class='grid-item'>";
								echo "<a href='http://localhost/rufos/index.php/animal/".$a->ID_Animal."' >";
								echo "<img src='/rufos/assets/fotos_animais/".$fotos[$a->ID_Animal]."' />";
								//echo "<button class='deleteAnimal' id='".$a->ID_Animal."'>Eliminar animal</button>";
								echo "</a></div>";
							}
						}
					
					</div>
					-->
				</div>
			</div>
		</div>

		<!--
		<div class="grid">
		  <div class="grid-item"><img src='/rufos/assets/fotos_animais/93_1.png'/></div>
		  <div class="grid-item grid-item-width2"><img src='/rufos/assets/fotos_animais/93_1.png'/></div>
		  <div class="grid-item"><img src='/rufos/assets/fotos_animais/93_1.png'/></div>
		</div>
		-->

		<!--
		<h1>Masonry - imagesLoaded progress, vanilla JS</h1>

		<div class="grid">
		  <div class="grid-sizer"></div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/orange-tree.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/submerged.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/look-out.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/one-world-trade.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/drizzle.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/cat-nose.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/contrail.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/golden-hour.jpg" />
		  </div>
		  <div class="grid-item">
		    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/flight-formation.jpg" />
		  </div>
		</div>
		-->


	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="/rufos/assets/bootstrap/js/bootstrap.min.js"></script>
	    <script src="/rufos/assets/JSfunctions.js"></script>
	    <script src="/rufos/assets/masonry.pkgd.min.js"/>
	    <script src="/rufos/assets/imagesloaded.pkgd.js"/>

	    <script>
			// external js: masonry.pkgd.js, imagesloaded.pkgd.js

			// init Masonry
			/*var $grid = $('.grid').masonry({
			  itemSelector: '.grid-item',
			  percentPosition: true,
			  columnWidth: '.grid-sizer'
			});*/
			// layout Isotope after each image loads
			/*$grid.imagesLoaded().progress( function() {
			  $grid.masonry();
			}); */


			$('.box1').imagesLoaded({
			    background: '.box2'
			  }, function( imgLoad ) {
			    $status.text( imgLoad.images.length + ' images loaded checking .box backgrounds' );
			  }
			);




			var $container = $('#image-container');

			var loadedImageCount, imageCount;

			$('#add').click( function() {
			  // add new images
			  var items = getItems();
			  $container.prepend( $(items) );
			  // use ImagesLoaded
			  $container.imagesLoaded()
			    .progress( onProgress );
			  // reset progress counter
			  imageCount = $container.find('img').length;
			});

			// -----  ----- //

			// return doc fragment with
			function getItems() {
			  var items = '';
			  for ( var i = 0; i < 7; i++ ) {
			    items += getImageItem();
			  }
			  return items;
			}

			// return an <li> with a <img> in it
			function getImageItem() {
			  var item = '<li class="is-loading">';
			  var size = Math.random() * 3 + 1;
			  var width = Math.random() * 110 + 100;
			  width = Math.round( width * size );
			  var height = Math.round( 140 * size );
			  var rando = Math.ceil( Math.random() * 1000 );
			  // 10% chance of broken image src
			  // random parameter to prevent cached images
			  var src = rando < 100 ? '//foo/broken-' + rando + '.jpg' :
			    // use lorempixel for great random images
			    '//lorempixel.com/' + width + '/' + height + '/' + '?' + rando;
			  item += '<img src="' + src + '" /></li>';
			  return item;
			}

			// -----  ----- //

			function resetProgress() {
			  $status.css({ opacity: 1 });
			  loadedImageCount = 0;
			  if ( supportsProgress ) {
			    $progress.attr( 'max', imageCount );
			  }
			}

			// triggered after each item is loaded
			function onProgress( imgLoad, image ) {
			  // change class if the image is loaded or broken
			  var $item = $( image.img ).parent();
			  $item.removeClass('is-loading');
			  if ( !image.isLoaded ) {
			    $item.addClass('is-broken');
			  }
			  // update progress element
			  loadedImageCount++;
			}
 
		</script>
  	</body>
</html>
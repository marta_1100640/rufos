<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>Bootstrap 101 Template</title>

	    <!-- Bootstrap -->
	    <link href="/rufos/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <link rel="stylesheet" href="/rufos/assets/css/stylesheet.css" type="text/css" media="screen"/>

	    <!--<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">-->

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>
  	<body>
  		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#"><img src='/rufos/assets/icons/rufos.png' style='height: 100%; width: 100%;'></a>
		    </div>
		    <ul class="nav navbar-nav navbar-right">
		      <li>
		      	<a href="/rufos/index.php/home">
		      		<img src='/rufos/assets/icons/ic_home_white_48dp_1x.png' data-toggle="tooltip" title="Página principal" style='height: 35px; width: auto; '/>
		      	</a>
		      </li>
		      <li>
		      	<a href="/rufos/index.php/pesquisa/associacao">
		      		<div class='esp'></div>
		      		<img src='/rufos/assets/icons/iconathon_animal-shelter_simple-black_48x48_2.png' data-toggle="tooltip" title="Pesquisar associação" style='height: 28px; width: auto; '/>
		      	</a>
		      </li>
		      <li>
		      	<a href="/rufos/index.php/pesquisa/animal">
		      		<div class='esp2'></div>
		      		<img src='/rufos/assets/icons/ic_pets_white_48dp_1x.png' data-toggle="tooltip" title="Pesquisar animal" style='height: 30px; width: auto; '/>
		      	</a>
		      </li>
		      <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown">
		        	<div class='esp'></div>
		        	<img src='/rufos/assets/icons/ic_account_circle_white_48dp_1x.png' data-toggle="tooltip" title="Conta" style='height: 30px; width: auto; '/>
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		          <li><a href="#">Notificações</a></li>
		          <li><a href="#">Favoritos</a></li>
		          <li><a href="#">Editar perfil</a></li>
		        </ul>
		      </li>
		      <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown">
		        	<img src='/rufos/assets/icons/ic_supervisor_account_white_48dp_1x.png' data-toggle="tooltip" title="Associação" style='height: 36px; width: auto; '/>
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		          <li><a href="#">Notificações</a></li>
		          <li><a href="/rufos/index.php/adicionar/animal">Adicionar animal</a></li>
		          <li><a href="#">Gerir animais</a></li>
		          <li><a href="#">Criar notícia</a></li>
		          <li><a href="#">Criar apelo externo</a></li>
		          <li><a href="#">Gerir cargos</a></li>
		          <li><a href="#">Editar perfil</a></li>
		        </ul>
		      </li>
		    </ul>
		  </div>
		</nav>
  		<div class="container-fluid">
  			<div class="row">
  					<?php
  						$base = base_url();
  						echo "<div class='background-image' style='background: url(".$base."assets/fotos_animais/".$fotoanimal[0].") no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;'></div>";
  					?>
					<div class="col-md-4 content1">
						<?php
							$contador=0;
							$base = base_url();
							if(isset($fotoanimal)){
								foreach($fotoanimal as $f){
									/*echo "<img src='<?php echo base_url('../assets/fotos_animais/".$f."'); ?>' />";*/
									//echo "<img src='".$base."assets/fotos_animais/".$f."' />";
									//echo "<div class='roundimage' style='background: url(".$base."assets/fotos_animais/".$f.");' />";
									echo "<div class='roundedImage unblur' style='background: url(".$base."assets/fotos_animais/".$f."); background-size: cover; background-repeat: no-repeat; background-position: 50%;'>&nbsp;</div>";
									$contador++;
								}
							}
						?>
					</div>
					<div class="col-md-4 content2">
						<?php
							if(isset($id)){
								
								echo "<span class='SFont'>NOME</span>";
								echo "</br><span class='nome LFont' id='".$id."'>".$nome."</span>";
								echo "<span class='espacamento'>";
								echo "</span>";
								echo "</br><span class='SFont'>ASSOCIAÇÃO</span>";
								echo "</br><span class='ong LFont' id='".$ongid."'>".$ongnome."</span>";
								echo "<span class='espacamento'>";
								echo "</span>";
								echo "</br><span class='SFont'>LOCALIZAÇÃO</span>";
								echo "</br><span class='localizacao LFont'>".$localizacao."</span>";
							}
						?>
					</div>
					<div class="col-md-4 content3">
						<?php
							if(isset($especie)){
								echo "<span class='SFont'>ESPÉCIE</span>";
								echo "</br><span class='especie LFont'>".$especie."</span>";
								echo "<span class='espacamento'>";
								echo "</span>";
								echo "</br><span class='SFont'>SEXO</span>";
								echo "</br><span class='sexo LFont'>".$sexo."</span>";
								echo "<span class='espacamento'>";
								echo "</span>";
								echo "</br><span class='SFont'>DATA DE NASCIMENTO</span>";
								echo "</br><span class='data LFont'>".$datanascimento."</span>";
							}
						?>
					</div>
			</div>


		    <div class="row content4">
				<div class="col-md-12" id="c3">

					<ul class="nav nav-tabs">
					  <li class="col-md-4 active"><a data-toggle="tab" href="#caracteristicas">Características fisicas</a></li>
					  <li class="col-md-4"><a data-toggle="tab" href="#outras">Outras</a></li>
					  <li class="col-md-4"><a data-toggle="tab" href="#descricao">Descrição</a></li>
					</ul>

					<div class="tab-content">
					  <div id="caracteristicas" class="tab-pane fade in active">
					    <?php
					    		echo "Estado: ".$estado;
								echo "</br>Adotado: ".$adotado;
								echo "</br>Porte: ".$porte;
								echo "</br>Orelhas: ".$orelhas;
								echo "</br>Cauda: ".$cauda;
								echo "</br>Comprimento do Pelo: ".$comprimentopelo;
								echo "</br>Tipo de pelo: ".$tipopelo;
								echo "</br>Padrão da cor: ".$padraocor;
								echo "</br>Focinho: ".$focinho;
								echo "</br>Olhos: ".$olhos;
								echo "</br>Cor: ";
								
								if(isset($cor)){
									$contador=0;
									foreach($cor as $c){
										if($contador==0){
											echo $c;
										}else{
											echo ", ".$c;
										}
										$contador++;
									}
								}
					    ?>
					  </div>
					  <div id="outras" class="tab-pane fade">
					    <?php
					    	echo "Temperamento: ";

								if(isset($temperamento)){
									$contador=0;
									foreach($temperamento as $t){
										if($contador==0){
											echo $t;
										}else{
											echo ", ".$t;
										}
										$contador++;
									}
								}

								echo "</br>Treino: ";

								if(isset($treino)){
									$contador=0;
									foreach($treino as $t){
										if($contador==0){
											echo $t;
										}else{
											echo ", ".$t;
										}
										$contador++;
									}
								}

								echo "</br>Situacao clinica: ";

								if(isset($situacaoclinica)){
									$contador=0;
									foreach($situacaoclinica as $sc){
										if($contador==0){
											echo $sc;
										}else{
											echo ", ".$sc;
										}
										$contador++;
									}
								}
					    ?>
					  </div>
					  <div id="descricao" class="tab-pane fade">
					    <?php
					    	echo "<p>".$descricao."</p>";
					    ?>
					  </div>
					</div>
					
				</div>
			</div>
		</div>

	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="/rufos/assets/bootstrap/js/bootstrap.min.js"></script>
	    <script src="/rufos/assets/JSfunctions.js"></script>
  	</body>
</html>
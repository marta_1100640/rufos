<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>Bootstrap 101 Template</title>

	    <!-- Bootstrap -->
	    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <link rel="stylesheet" href="../assets/css/stylesheet.css" type="text/css" media="screen"/>

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>
  	<body>
  		<div class="container-fluid">
  			<div class="row">
  				<div class="col-md-12" id="c1">Header</div>
  			</div>
		    <div class="row">
				<div class="col-md-2" id="c1">
					Coluna 1
					</br>
					</br>
					<a href="http://localhost/rufos/index.php/search">Pesquisar</a>
					</br>
					<a href="http://localhost/rufos/index.php/myong">My ONG</a>
				</div>
				<div class="col-md-10" id="c2">
					MY ONG
					</br>
					</br>
					<a href="http://localhost/rufos/index.php/addanimal">Adicionar animal</a>
				</div>
			</div>
		</div>

	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
	    <script src="../assets/JSfunctions.js"></script>
  	</body>
</html>